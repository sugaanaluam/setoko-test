# SETOKO-TEST


## INSTALASI

## Download Source

- Buka direktori yang akan digunakan untuk menyimpan source service
- Clone source ke local kemudian masuk ke direktori source, dengan perintah:
```
$ git clone https://gitlab.com/sugaanaluam/setoko-test.git
$ cd setoko-test
``` 

## Direct Script

- Build source menjadi executable file, dengan perintah :
```
$ go mod tidy
$ go build -o setoko-maps-service
```

- Kemudian jalankan service dengan perintah :
```
$ ./setoko-maps-service
```
Service akan secara otomatis merujuk kepada file konfigurasi yang berada pada direktori ./configs/.configs.yaml
jika akan menggunakan file config secara tersendiri, dapat membuat OS environtment variabel dengan perintah :
```
$ export APP_CONFIG=/path/to/config
```

## Menjalankan dengan Docker

- Untuk menjalankan service dengan menggunakan docker kita perlu melakukan build image, pada source direktori gunakan perintah :
```
$ docker build -t setoko-maps-service -f Dockerfile .
```
Proses ini, biasanya memerlukan waktu untuk sampai terbentuk docker image. untuk memeriksa docker image yang sudah di-build, kita bisa gunakan perintah :
```
$ docker image ls
```

- Jalankan container dengan docker-compose menggunakan perintah:
```
$ docker-compose -f docker-compose.yml up -d
```

- Untuk menghentikan container, gunakan perintah:
```
$ docker-compose -f docker-compose.yml down
```

## Test API
- Untuk menguji API yang sudah berjalan kita dapat mennggunakan command line cURL dengan perintah:
```
$ curl --location 'localhost:8100/api/v1/maps/distance/walking?origin=107.619832%2C-6.935519&destination=107.636076%2C-6.926715'
```
