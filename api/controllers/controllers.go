package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/sugaanaluam/setoko-test/configs"
	"gitlab.com/sugaanaluam/setoko-test/utils"
)

type Maps struct{}

type MapboxResponse struct {
	Code   string     `json:"code"`
	Routes []*Routes `json:"routes"`
}

type Routes struct {
	Distance float64 `json:"distance"`
	Duration float64 `json:"duration"`
}

func (a *Maps) GetDistance(w http.ResponseWriter, r *http.Request) {
	routingProfile := mux.Vars(r)["routing"]
	query := r.URL.Query()

	origin := query.Get("origin")
	destination := query.Get("destination")

	if routingProfile == "" {
		utils.Log.Println("| ERROR Parameter | Routing Profile not found ")
		http.Error(w, "Choose a Routing Profile driving OR cycling OR walking", http.StatusBadRequest)
		return
	}

	if origin == "" || destination == "" {
		utils.Log.Println("| ERROR Parameter | Missing required parameters ")
		http.Error(w, "Missing required parameters", http.StatusBadRequest)
		return
	}

	resp, err := http.Get(fmt.Sprintf("%s/%s/%s;%s?access_token=%s", configs.Properties.ThirdParty.MapBox.DirectionAPI, routingProfile, origin, destination, configs.Properties.ThirdParty.MapBox.Token))
	if err != nil {
		utils.Log.Printf("| ERROR API Request | %s\n", err.Error())
		http.Error(w, "Error getting data from Mapbox API", http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Log.Println("| ERROR API Response | error reading response")
		http.Error(w, "Error reading response from Mapbox API", http.StatusInternalServerError)
		return
	}
	var mapboxResponse MapboxResponse
	err = json.Unmarshal(body, &mapboxResponse)
	if err != nil {
		utils.Log.Println("| ERROR API Response | error unmarshal response")
		http.Error(w, "Error JSON unmarshaling response from Mapbox API", http.StatusInternalServerError)
		return
	}
	if mapboxResponse.Code != "Ok" || len(mapboxResponse.Routes) < 1 {
		utils.Log.Println("| ERROR API Response | cannot find distanse")
		http.Error(w, "Error cannot find distance", http.StatusInternalServerError)
		return
	}

	distance := mapboxResponse.Routes[0].Distance
	distanceString := strconv.FormatFloat(distance/1000, 'f', 2, 64)
	
	responseStruct := map[string]interface{}{
		"distance" : distanceString,
		"duration" : mapboxResponse.Routes[0].Duration,
	}
	response, _ := json.Marshal(responseStruct)

	utils.Log.Println("| Success Get Response")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)

}
