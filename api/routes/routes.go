package routes

import (
	"log"
	"net/http"

	muxHandlers "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"gitlab.com/sugaanaluam/setoko-test/utils"
)

// Routes :
type Routes struct {
	Router *mux.Router
	Port   string
}

// InitializeRoutes :
func (a *Routes) Initialize() {
	a.Router = mux.NewRouter()
	a.Router.StrictSlash(false)
	a.Router.PathPrefix("/").Subrouter()
	a.ListRouter()

	negroWare := negroni.New()
	logger := negroni.NewLogger()
	logger.ALogger = utils.Log
	logger.SetFormat("| {{.Status}} | \t {{.Duration}} | {{.Hostname}} | {{.Method}} {{.Path}}")
	negroWare.Use(logger)

	negroWare.UseHandler(a.Router)

	http.DefaultTransport.(*http.Transport).MaxIdleConnsPerHost = 1000

	log.Fatal(http.ListenAndServe(":"+a.Port, muxHandlers.CORS()(negroWare)))
}
