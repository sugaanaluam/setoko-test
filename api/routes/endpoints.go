package routes

import (
	"net/http"

	"gitlab.com/sugaanaluam/setoko-test/api/controllers"
)

func (a *Routes) ListRouter() {

	mapsRoutes := a.Router.PathPrefix("/api/v1/maps").Subrouter()

	maps := controllers.Maps{}

	mapsRoutes.Handle("/distance/{routing}", http.HandlerFunc(maps.GetDistance)).Methods("GET")

}
