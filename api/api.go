package api

import (
	"log"

	"gitlab.com/sugaanaluam/setoko-test/api/routes"
	"gitlab.com/sugaanaluam/setoko-test/configs"
	"gitlab.com/sugaanaluam/setoko-test/utils"
)

type Api struct{}

func Start() error {
	config := configs.Properties.API
	utils.Log.Printf("| API | Starting | Port: %s\n", config.Port)
	log.Printf("| API | Started at Port: %s\n", config.Port)
	apiRoutes := routes.Routes{Port: config.Port}
	apiRoutes.Initialize()

	return nil
}