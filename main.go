package main

import (
	"log"

	"gitlab.com/sugaanaluam/setoko-test/api"
	"gitlab.com/sugaanaluam/setoko-test/app"
	"gitlab.com/sugaanaluam/setoko-test/configs"
)

func main() {

	err := app.Initialize("Setoko-Test-API")
	if err != nil {
	ReInitialize:
		app.Sleeping()
		err := app.Initialize("Setoko-Test-API")
		if err != nil {
			goto ReInitialize
		}
	}

	if configs.Properties.API.Status {
		err = api.Start()
		if err != nil {
			log.Printf("| API | Error: %s\n", err.Error())
		}

	}
	
}
