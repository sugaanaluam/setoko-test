package configs

import (
	"strings"

	"github.com/spf13/viper"
)

// ConfigStruct : struct config
type ConfigStruct struct {
	Version string  `mapstructure:"version"`
	API         APIConfig         `mapstructure:"api"`
	LumberjackLogger LumberjackLoggerConfig `mapstructure:"lumberjackLogger"`
	ThirdParty		ThirdPartyConfig	`mapstructure:"thirdparty"`
}

type APIConfig struct {
	Port   string `mapstructure:"port"`
	Status bool   `mapstructure:"status"`
}

type LumberjackLoggerConfig struct {
	DailyRotate bool `mapstructure:"dailyRotate"`
	CompressLog bool `mapstructure:"compressLog"`
}

type ThirdPartyConfig struct {
	MapBox	MapBoxConfig	 `mapstructure:"mapbox"`
}

type MapBoxConfig struct {
	DirectionAPI string `mapstructure:"directionapi"`
	Token string `mapstructure:"token"`
}

var Properties ConfigStruct

func InitConfig(configPath, serviceName string) error {
	viper.AddConfigPath("./")
	viper.AddConfigPath("./configs/")
	viper.SetConfigName(".configs")
	viper.SetConfigFile(configPath)

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	envPrefix := "TESTSERVICE"

	if serviceName != "" {
		envPrefix = strings.ToUpper(serviceName)
		envPrefix = strings.ReplaceAll(envPrefix, " ", "_")
	}

	viper.SetEnvPrefix(envPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	err := viper.Unmarshal(&Properties)
	if err != nil {
		return err
	}

	return nil
}