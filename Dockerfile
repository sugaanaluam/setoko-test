FROM golang:1.17.5-stretch

ENV GOLANG_VERSION=1.17 \
    GOPATH="/go/bin/app"

RUN mkdir -p /go/src/app/setoko-maps-service

WORKDIR /go/src/app/setoko-maps-service
COPY . .

RUN mkdir -p /go/bin/app/setoko-maps-service

RUN apt-get install git \
    && go mod download \
    && go build -o /go/bin/app/setoko-maps-service/main

CMD ["/go/bin/app/setoko-maps-service/main"]