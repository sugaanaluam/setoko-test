package app

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"gitlab.com/sugaanaluam/setoko-test/configs"
	"gitlab.com/sugaanaluam/setoko-test/utils"
)

var (
	ServiceName string
	ConfigPath string
	LogPath    string
)

func getEnv() {
	ConfigPath = os.Getenv("APP_CONFIG")
	LogPath = os.Getenv("APP_LOG")

}

func Initialize(args ...string) error {
	getEnv()

	if len(args) > 0 {
		ServiceName = args[0]
	}

	err := configs.InitConfig(ConfigPath, ServiceName)
	if err != nil {
		log.Println("| InitConfig |", err.Error())
		return err
	}

	err = SetLog(ServiceName, LogPath)
	if err != nil {
		log.Println("| SetLog |", err.Error())
		return err
	}

	if ServiceName != "" {
		fmt.Printf("....Starting %s %s....\n", args[0], configs.Properties.Version)

	}

	utils.Log.Printf("....Starting %s %s....\n", args[0], configs.Properties.Version)

	SetupCloseHandler()

	return nil
}

func SetLog(serviceName, logPath string) error {
	if logPath == "" {
		logPath = fmt.Sprintf("%s%s%s", "./log/", strings.ToLower(strings.ReplaceAll(serviceName, " ", "_")), ".log")
	}

	err := (&utils.LumberjackLogger{
		LogPath:     logPath,
		DailyRotate: configs.Properties.LumberjackLogger.DailyRotate,
		CompressLog: configs.Properties.LumberjackLogger.CompressLog,
	}).SetLog()
	if err != nil {
		return err
	}

	return nil
}

func GetVersion() string {
	fmt.Println(configs.Properties.Version)
	return configs.Properties.Version
}

// SetupCloseHandler :
func SetupCloseHandler() {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("\r- Ctrl+C pressed in Terminal")
		os.Exit(0)
	}()
}

func Sleeping() {
	sleep := 5

	fmt.Println("....Retry in", sleep, "seconds....")

	time.Sleep(time.Duration(sleep) * time.Second)
}